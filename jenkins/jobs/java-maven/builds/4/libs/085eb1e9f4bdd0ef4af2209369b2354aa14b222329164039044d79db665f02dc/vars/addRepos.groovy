#!/usr/bin/env groovy

def call() {
    if (BRANCH_NAME == null) {
        error "BRANCH_NAME environment variable not set"
    }
    else if (BRANCH_NAME.startsWith("feature/") || BRANCH_NAME == "develop" || BRANCH_NAME == "preprod") {
        return "44.200.246.63:8083/dev"
    } else if (BRANCH_NAME == "main") {
        return "44.200.246.63:8083/prod"
    } else {
        error "Unsupported branch: ${BRANCH_NAME}"
    }
}