#!/usr/bin/env groovy

def call() {
    sh 'mvn package'
    echo "the build stage for ${BRANCH_NAME}"
}

