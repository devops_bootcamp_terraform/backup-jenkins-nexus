#!/usr/bin/env groovy

def call() {
    if (BRANCH_NAME == null) {
        error "BRANCH_NAME environment variable not set"
    }
    else if (BRANCH_NAME.startsWith("feature/") || BRANCH_NAME == "develop" || BRANCH_NAME == "preprod") {
        return "${BRANCH_NAME}-SNAPSHOT-${BUILD_NUMBER}"
    } else if (BRANCH_NAME == "main") {
        return "1.0.${BUILD_NUMBER}"
    } else {
        error "Unsupported branch: ${env.BRANCH_NAME}"
    }
}