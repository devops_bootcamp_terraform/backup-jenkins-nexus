#!/usr/bin/env groovy

def call() {
  withCredentials([
        usernamePassword(credentialsId: 'nexus-cred', usernameVariable: 'USER', passwordVariable: 'PWD')
    ]) {
            sh "echo $PWD | docker login -u $USER --password-stdin ${env.REGISTRY}"
           
        }  
}